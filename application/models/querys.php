<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    //Variables de 6x1 para el calculo de el 6x1
    var $referidos = array();
    
    function __construct()
    {
        parent::__construct();
    } 
    
    function getBanner(){
        $this->db->order_by('priority','ASC');
        return $this->db->get('banner');
    }
    
    function getCategorias(){
        $this->db->order_by('orden','ASC');
        return $this->db->get('categorias');
    }
    
    function getProductos(){
        if(!empty($_GET['categoria']) && is_numeric($_GET['categoria']) && $_GET['categoria']>0){
            $this->db->where('categorias_id',$_GET['categoria']);
        }
        if(!empty($_GET['producto'])){
            $this->db->like('producto_nombre',$_GET['producto']);
        }
        $this->db->order_by('priority','ASC');
        $this->db->where('disponible',1);
        return $this->db->get('productos');
    }
    
    function getCarrito(){
        if(empty($_SESSION['carrito'])){
            return array();
        }
        else{
            return $_SESSION['carrito'];
        }
    }
    
    function setCarrito($item,$sumarcantidad = TRUE){
        if(empty($_SESSION['carrito'])){
            $_SESSION['carrito'] = array();
        }
        $existente = false;
        foreach($_SESSION['carrito'] as $n=>$c){
            if($c->id==$item->id){
                if($sumarcantidad){
                    $_SESSION['carrito'][$n]->cantidad+=$item->cantidad;
                }else{
                    $_SESSION['carrito'][$n]->cantidad=$item->cantidad;
                }
                if(!empty($item->gramajeselected)){
                    $_SESSION['carrito'][$n]->gramajeselected=$item->gramajeselected;
                }
                $existente = true;
            }
        }
        if(!$existente){
            array_push($_SESSION['carrito'],$item);
        }        
    }
    
    function delCarrito($producto_id){
        if(empty($_SESSION['carrito'])){
            $_SESSION['carrito'] = array();
        }        
        foreach($_SESSION['carrito'] as $n=>$c){
            if($c->id==$producto_id){
                unset($_SESSION['carrito'][$n]);
            }
        }                
    }
    
    function getPage($titulo,$return = FALSE){
        $texto = $this->db->get_where('paginas',array('titulo'=>$titulo));
        $str = '';
        if($texto->num_rows()>0){
            $str = $texto->row()->texto;
        }
        
        if($return){
            return $str;
        }
        else{
            echo $str;
        }
    }
    
    function getComentarios($productos_id){
        return $this->db->get_where('comentarios',array('productos_id'=>$productos_id));
    }
    
    function pagoOk($post,$procesado = 2){
        if(!empty($_SESSION['productocompra'])){                                        
            $this->db->select('
                ventas.id,
                user.nombre as "nom",
                user.apellido_paterno as "cognoms",
                user.email as "email",
                user.ciudad as "ciutat",
                provincias.nombre_provincia as "provincia",
                user.direccion as "adreça",
                user.codigo_postal as "CP",
                user.telefono as "tel",                
                ventas.total as total_compra,
                ventas.fecha_compra as "Data compra", 
                ventas.dia_entrega as "Dia d\'entrega",
                ventas.hora_entrega as "hora d\'entregament", 
                ventas.forma_pago as "Forma de pagament", 
                ventas.costo_envio as "Cost enviament", 
                ventas.observaciones as Observacions, 
                productos.producto_nombre as Producte, 
                ventas_detalles.cantidad as Quantitat, 
                ventas_detalles.monto as Import, 
                ventas_detalles.gramaje as Gramatge
            ');            
            $this->db->join('user','user.id = ventas.user_id');
            $this->db->join('provincias','user.provincias_id = provincias.id');
            $this->db->join('ventas_detalles','ventas.id = ventas_detalles.ventas_id');
            $this->db->join('productos','productos.id = ventas_detalles.productos_id');
            $cliente = $this->db->get_where('ventas',array('ventas.transactionid'=>$_SESSION['productocompra']));
            if($cliente->num_rows()>0){
                $varc = '';
                $var = '<h1>Nova comanda feta per '.$cliente->row()->nom.' '.$cliente->row()->cognoms.'</h1>';
                $var.= '<h2>Dades comprador</h2>';
                foreach($cliente->row() as $n=>$v){
                     if($n=='total_compra'){
                         $var.= '<h1>La teva Comanda</h1>';
                         $varc.= '<h1>La teva Comanda</h1>';
                     }
                     if($n=='Observacions'){
                         $var.= '<p><b>'.$n.'</b>: '.$v.'</p>';
                         if(!empty($varc)){
                            $varc.= '<p><b>'.$n.'</b>: '.$v.'</p>';
                         }
                         $var.= '<h1>Productes</h1>';
                         $varc.= '<h1>Productes</h1>';
                         $this->db->select('productos.producto_nombre as Producte, 
                            ventas_detalles.cantidad as Quantitat, 
                            ventas_detalles.monto as Import, 
                            ventas_detalles.gramaje as Gramatge');
                         $this->db->join('productos','productos.id = ventas_detalles.productos_id');
                         foreach($this->db->get_where('ventas_detalles',array('ventas_id'=>$cliente->row()->id))->result() as $r){
                             foreach($r as $p=>$d){
                                $varc.= '<p><b>'.$p.'</b>: '.$d.'</p>';
                                $var.= '<p><b>'.$p.'</b>: '.$d.'</p>';
                             }
                         }
                     }else{
                         $var.= '<p><b>'.$n.'</b>: '.$v.'</p>';
                         if(!empty($varc)){
                            $varc.= '<p><b>'.$n.'</b>: '.$v.'</p>';
                         }
                     }
                     /*if(!empty($varc)){
                         $varc.= '<p><b>'.$n.'</b>: '.$v.'</p>';
                     }
                     $var.= '<p><b>'.$n.'</b>: '.$v.'</p>';*/
                }
                $var.= '<h2>Comanda realitzada</h2>';
                foreach($post as $n=>$v){
                    if(!is_array($v)){
                        $var.= '<p><b>'.$n.'</b>: '.$v.'</p>';
                    }
                }
                
                $varc = 'Hola '.$cliente->row()->nom.' '.$cliente->row()->cognoms.' gracies per la teva compra, per qualsevol dubte a <a href="mailto:carndequalitat@canmabres.com">carndequalitat@canmabres.com</a> o al telèfon <a href="tel:690608194">690 608 194</a>'.$varc.'<p align="center"><a href="http://www.canmabres.com">www.canmabres.com</a></p>';
                //Envio al vendedor                
                correo('joncar.c@gmail.com','Nova Comanda',$var);
                correo('info@hipo.tv','Nova Comanda',$var);
                correo('carndequalitat@canmabres.com','Nova Comanda',$var);
                //Envio al comprador
                //if($procesado==2){
                    correo($cliente->row()->email,'Gracies per la seva comanda',$varc);
                    correo('joncar.c@gmail.com','Gracies per la seva comanda',$varc);
                    correo('info@hipo.tv','Gracies per la seva comanda',$varc);
                    correo('carndequalitat@canmabres.com',$varc);
                //}
                
            }
        }
        unset($_SESSION['productocompra']);
    }
    
    function pagoNoOk(){
        if(!empty($_SESSION['productocompra'])){                            
            $this->db->update('ventas',array('procesado'=>-1),array('id'=>$_SESSION['productocompra']));
            unset($_SESSION['productocompra']);
        }
    }
}
?>
