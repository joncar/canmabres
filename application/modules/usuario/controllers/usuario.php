<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Usuario extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function ventas(){
            $crud = $this->crud_function('','');            
            $crud->where('user_id',$this->user->id);
            $crud->set_subject('Les teves compres');
            $crud->unset_add()
                ->unset_edit()
                ->unset_read()
                ->unset_export()
                ->unset_print()
                ->display_as('Datos de entrega')                
                ->columns('procesado','fecha_compra','productos','dia_entrega','hora_entrega','provincias_id','forma_pago','total');
            
            $crud->display_as('fecha_compra','Data de compra')
                 ->display_as('procesado','Procesat')
                 ->display_as('productos','Productes')
                 ->display_as('provincias_id','Provincia');
            
            $crud->callback_column('procesado',function($val,$row){
                switch($val){
                    case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                    case '1': return '<a href="'.site_url('usuario/comprar/'.$row->id).'" class="label label-default">Per procesat</a>'; break;
                    case '2': return '<span class="label label-success">Procesat</span>'; break;
                }
            });
            
            $crud->callback_column('total',function($val,$row){
                return moneda($val);
            });
            
            $crud->callback_column('productos',function($val,$row){
                get_instance()->db->join('productos','productos.id = ventas_detalles.productos_id');
                $ventas = get_instance()->db->get_where('ventas_detalles',array('ventas_id'=>$row->id));
                $str = '';
                foreach($ventas->result() as $v){
                    $str.= $v->cantidad.' '.$v->producto_nombre.',';
                }
                return $str;
            });
            $output = $crud->render();
            $output->title = 'Les teves compres';
            $this->loadView($output);
        }    
        
        function comprar($id = ''){
            $transaction = rand(0,2000).date("is");
            if(!empty($_SESSION['carrito']) && empty($id)){
                //print_r($_SESSION['carrito']);
                $data = array('user_id'=>$this->user->id,'total'=>0,'fecha_compra'=>date("Y-m-d H:i:s"),'procesado'=>1);
                $data['provincias_id'] = $_SESSION['envio']['provincias_id'];
                $data['dia_entrega'] = $_SESSION['envio']['dia_entrega'];
                $data['hora_entrega'] = $_SESSION['envio']['hora_entrega'];
                $data['forma_pago'] = $_SESSION['envio']['forma_pago'];
                $data['observaciones'] = $_SESSION['envio']['observaciones'];
                $data['costo_envio'] = $_SESSION['envio']['costo_envio'];
                $this->db->insert('ventas',$data);
                unset($_SESSION['envio']);
                $id = $this->db->insert_id();
                $total = 0;
                foreach($_SESSION['carrito'] as $n=>$v){
                    $monto = ($v->precio*$v->cantidad);
                    $monto = number_format($monto,2,'.',',');
                    $this->db->insert('ventas_detalles',array('gramaje'=>$v->gramajeselected,'ventas_id'=>$id,'productos_id'=>$v->id,'cantidad'=>$v->cantidad,'monto'=>$monto));
                    $total+= ($v->precio*$v->cantidad);
                }
                $total+= $data['costo_envio'];
                $total = number_format($total,2,'.',',');
                $this->db->update('ventas',array('total'=>$total,'transactionid'=>$transaction),array('id'=>$id));
                $_SESSION['productocompra'] = $transaction;
                if($data['forma_pago']!='Visa'){
                    $this->querys->pagoOk(array(),1);
                }
                unset($_SESSION['carrito']);
                $this->loadView(array('view'=>'pagar','venta'=>$this->db->get_where('ventas',array('id'=>$id))->row(),'submit'=>true));
            }else{
                if(empty($id) || !is_numeric($id)  || $id<0){
                    header("Location:".base_url('panel'));
                }else{
                    $venta = $this->db->get_where('ventas',array('id'=>$id));
                    if($venta->num_rows()>0){                        
                        $this->db->update('ventas',array('transactionid'=>$transaction),array('id'=>$id));
                        $venta->row()->transactionid = $transaction;
                        $this->loadView([
                            'view'=>'panel',
                            'crud'=>'user',
                            'output'=>$this->load->view('pagar',['venta'=>$venta->row(),'redsys'=>$this->redsysapi,'submit'=>false],TRUE)
                        ]);
                    }else{
                        header("Location:".base_url('panel'));
                    }
                }
            }            
        }                
    }
?>
