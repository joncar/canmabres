<?php
    require_once APPPATH.'/controllers/Panel.php';    
    class Banner extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->library('image_crud');
        }
        
        function img(){
            $crud = new image_CRUD();
            $crud->set_table('banner')
                    ->set_url_field('foto')
                    ->set_image_path('images/banner')
                    ->set_ordering_field('priority')
                    ->module = 'admin';
            $output = $crud->render();
            $output->output.= '<div style="margin:30px"><b>Nota: </b>Se recomienda que las fotos subidas como banner sean de 1920x1080</div>';
            $this->loadView($output);
        }
    }
?>
