<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class ProductosCtrl extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->library('image_crud');
        }
        
        function categorias(){
            $crud = $this->crud_function('','');
            $crud->add_action('<i class="fa fa-"></i> Productos','',base_url('admin/productosctrl/productos').'/');
            $crud->set_field_upload('foto','images/categorias');
            $crud->columns('categoria_nombre','productos');
            $crud->callback_column('productos',function($val,$row){
                return (string)get_instance()->db->get_where('productos',array('categorias_id'=>$row->id))->num_rows();
            });
            $crud->unset_delete();
            $this->loadView($crud->render());
        }
        function productos($categoria_id = ''){
            if(!empty($categoria_id)){
                $crud = $this->crud_function('','');
                $crud->where('categorias_id',$categoria_id);
                $crud->field_type('categorias_id','hidden',$categoria_id);
                $crud->field_type('gramaje','tags');
                $crud->columns('foto','producto_nombre','gramaje','precio','precio_sin_descuento','disponible');
                $crud->set_field_upload('foto','images/productos');
                $crud->add_action('<i class="fa fa-image"></i> Imagenes','',base_url('admin/productosctrl/fotos').'/');
                $crud->add_action('<i class="fa fa-image"></i> Comentarios','',base_url('admin/productosctrl/comentarios').'/');
                $crud->set_clone();
                $crud->callback_before_delete(function($id){
                    if(get_instance()->db->get_where('ventas_detalles',array('productos_id'=>$id))->num_rows()>0)
                        return false;
                    return true;
                });
                $this->loadView($crud->render());
            }else{
                header("Location:".base_url('admin/productosctrl/categorias/'));
            }
        }
        
        function fotos($categoria_id = ''){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('fotos')                        
                ->set_image_path('images/productos')
                ->set_relation_field('productos_id')
                ->set_ordering_field('priority')
                ->set_url_field('foto')
                ->module = 'admin';
            $this->loadView($crud->render());
        }
        
        function comentarios($productos_id = ''){
            if(!empty($productos_id)){
                $crud = $this->crud_function('','');
                $crud->where('productos_id',$productos_id);                
                $crud->unset_columns('productos_id');
                $this->loadView($crud->render());
            }else{
                header("Location:".base_url('admin/productosctrl/categorias/'));
            }
        }
    }
?>
