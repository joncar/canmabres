<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Pages extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->library('image_crud');
        }
        
        function paginas(){
            $crud = $this->crud_function('','');
            $crud->field_type('texto','editor',array('type'=>'text_editor'));
            $crud->unset_columns('texto');
            $this->loadView($crud->render());
        }
        
        function ganaderia(){
            $this->norequireds = array('pre','doma','video');
            $crud = $this->crud_function('','');
            $crud->field_type('tipo_publicacion','dropdown',array('1'=>'Nuestra ganaderia','2'=>'En venta','3'=>'Our Stallions','4'=>'For Sale'))
                 ->field_type('sexo','dropdown',array('M'=>'Masculino','F'=>'Femenino'))
                 ->field_type('descripcion','editor',array('type'=>'text_editor','editor'=>'ckeditor'))
                 ->set_field_upload('foto','images/fotos_caballos')
                 ->add_action('<i class="fa fa-image"></i> Imagenes','',base_url('admin/pages/imagenes_list').'/');
            
            $crud->callback_field('foto_url',function($val){
                return form_input('foto_url',$val,'id="field-foto_url" class="form-control"').'<a href="'.base_url('admin/tablas/banco_imagenes').'" target="_new" class="btn btn-info"><i class="fa fa-image"></i> Banco de imágenes</a>';
            });
            $crud->columns('tipo_publicacion','nombre','colores_id','ubicaciones_id','ejemplares_id');
            $this->loadView($crud->render());
        }
        
        function noticias_list(){            
            $this->as = array('noticias_list'=>'noticias');
            $this->norequireds = array('imagen');
            $crud = $this->crud_function('','');            
            $crud->set_subject('Noticias')
                     ->field_type('texto','editor',array('type'=>'text_editor','editor'=>'ckeditor'))
                     ->field_type('idioma','dropdown',array('en'=>'English','es'=>'Español'))
                     ->set_field_upload('imagen','images/fotos_noticias');
            $crud->callback_field('imagen_url',function($val){
                return form_input('imagen_url',$val,'id="field-imagen_url" class="form-control"').'<a href="'.base_url('admin/tablas/banco_imagenes').'" target="_new" class="btn btn-info"><i class="fa fa-image"></i> Banco de imágenes</a>';
            });
            $crud->add_action('<i class="fa fa-envelope"></i> Enviar a subscritos','',base_url('admin/pages/sendBoletin/noticias/').'/');
            $this->loadView($crud->render());
        }
        
        function consejos_list(){            
            $this->as = array('consejos_list'=>'consejos');
            $this->norequireds = array('imagen');
            $crud = $this->crud_function('','');            
            $crud->set_subject('Consejos')
                     ->field_type('texto','editor',array('type'=>'text_editor','editor'=>'ckeditor'))
                     ->field_type('idioma','dropdown',array('en'=>'English','es'=>'Español'))
                     ->set_field_upload('imagen','images/fotos_consejos');
            $crud->add_action('<i class="fa fa-envelope"></i> Enviar a subscritos','',base_url('admin/pages/sendBoletin/consejos/').'/');
            $crud->callback_field('imagen_url',function($val){
                return form_input('imagen_url',$val,'id="field-imagen_url" class="form-control"').'<a href="'.base_url('admin/tablas/banco_imagenes').'" target="_new" class="btn btn-info"><i class="fa fa-image"></i> Banco de imágenes</a>';
            });
            $this->loadView($crud->render());
        }
        
        function imagenes_list(){
            $crud = new image_CRUD();
            $crud->set_table('fotos_caballos')
                    ->set_url_field('img')
                    ->set_image_path('images/fotos_caballos')
                    ->set_ordering_field('priority')
                    ->set_relation_field('caballo')
                    ->module = 'admin';
            $this->loadView($crud->render());
        }
        
        function sendBoletin($table,$id){
            $boletin = $this->db->get_where($table,array('id'=>$id))->row();
            $str = $this->load->view('includes/boletin',array('table'=>$table,'id'=>$id,'boletin'=>$boletin),TRUE);
            foreach($this->db->get('subscribe')->result() as $s){
                correo($s->email,'Yeguadaseñorafina ha publicado algo nuevo',$str);
            }
            echo 'El mensaje ha sido enviado';
        }
    }
?>
