<!DOCTYPE html>
<html lang="en-US">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= empty($title)?'Canmabres':$title ?></title>               
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/plugins.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/styles.css?v=1.2">
        <link rel="stylesheet" href="<?= base_url() ?>css/style.css?v=1.2">
        <script src="<?= base_url() ?>js/vendor.js"></script>
        <script>
            window.SHOW_LOADING = false;
        </script>
        <?php 
        if(!empty($css_files)):
        foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
        <?php endforeach; ?>
        <?php endif; ?>
</head>

<body class="blog wpb-js-composer js-comp-ver-4.5.3 vc_responsive" >  
    <!-- // LOADING -->
    <div class="awe-page-loading">
        <div class="awe-loading-wrapper">
            <div class="awe-loading-icon">
                <?= img('img/logo.jpg','width:200px') ?>
            </div>

            <div class="progress">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
    </div>
    <!-- // END LOADING -->
    <div id="wrapper" class="main-wrapper ">
        <?php $this->load->view($view) ?>
    </div>
    <!-- /.login-popup -->
    <script>
        $(function() {
            $('a[href="#login-popup"]').magnificPopup({
                type:'inline',
                midClick: false,
                closeOnBgClick: false
            });
        });
    </script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>   
    <script src="<?= base_url() ?>js/admin/frame.js"></script>
    <script src="<?= base_url() ?>js/plugins.js"></script>
    <script src="<?= base_url() ?>js/main.js"></script>
    <script src="<?= base_url() ?>js/docs.js"></script>
    <script>
        function addToCart(producto_id,cantidad){
            $.post('<?= base_url() ?>main/addToCart/'+producto_id+'/'+cantidad,{},function(data){
                $(".menubar-cart").html(data);
                $("#header nav").removeClass('headroom--unpinned').addClass('headroom--pinned'); 
                $(".menubar-cart ul.submenu").addClass('showedcart');
                $(".menubar-cart ul.submenu").mouseout(function(){$(this).removeClass('showedcart');});
            });
        }
        function remToCart(producto_id){
            $.post('<?= base_url() ?>main/delToCart/'+producto_id,{},function(data){
                $(".menubar-cart").html(data);
            }); 
        }
    </script>
</body>
</html>

