<?php if(empty($_SESSION['user'])): ?>
<?php if(!empty($msj))echo $msj ?>
<?php if(!empty($_SESSION['msj']))echo $_SESSION['msj'] ?>
<form role="form" class="well" action="<?= base_url('main/login') ?>" onsubmit="return validar(this)" method="post">
   <h1 align="center" style="color:black; font-size:27.9px;">Accedeix al teu compte</h1>
   <?= input('email','Email de contacte','email') ?>
   <?= input('pass','Contrasenya','password') ?>   
   <?php if(empty($_GET['evento'])): ?>
   <input type="hidden" name="redirect" value="<?= empty($_GET['redirect'])?base_url('panel'):base_url($_GET['redirect']) ?>">   
   <?php else: ?>
   <input type="hidden" name="redirect" value="<?= base_url('usuario/inscribir/'.$_GET['evento']) ?>">   
   <?php endif ?>
   <div align="center" style="color:black"><input type="checkbox" name="remember" value="1" checked> Recordar contrasenya</div>
   <div align="center"><button type="submit" class="btn btn-success">ENTRAR</button>
   <!--<a class="btn btn-link" href="<?= base_url('registro/index/add') ?>">Registrate</a><br/>-->
   <a class="btn btn-link" href="<?= base_url('registro/forget') ?>">Has oblidat la contrasenya?</a>
   </div>
</form>
<?php else: ?>
<div align="center"><a href="<?= base_url('panel') ?>" class="btn btn-success btn-large">ENTRAR AL TEU COMPTE</a></div>
<?php endif; ?>
<?php $_SESSION['msj'] = null ?>
