<?php $this->load->view('includes/template/header') ?>
<?php 
    $detail = '';
    if(!empty($_GET['Ds_MerchantParameters'])){
        $this->load->library('redsysapi');            
        $miObj = new RedsysAPI;
        $decodec = json_decode($miObj->decodeMerchantParameters($_GET['Ds_MerchantParameters'])); 
        switch($decodec->Ds_Response){
            case '101': $detail = 'TARJETA CADUCADA'; break;
            case '102': $detail = 'TARJETA BLOQUEDA TRANSITORIAMENTE O BAJO SOSPECHA DE FRAUDE'; break;
            case '104': $detail = 'OPERACIÓN NO PERMITIDA'; break;
            case '106': $detail = 'NUM. INTENTOS EXCEDIDO'; break;
            case '107': $detail = 'CONTACTAR CON EL EMISOR'; break;
            case '109': $detail = 'IDENTIFICACIÓN INVALIDA DEL COMERCIO O TERMINAL'; break;
            case '110': $detail = 'IMPORTE INVALIDO'; break;
            case '114': $detail = 'TARJETA NO SOPORTA'; break;
            case '116': $detail = 'DISPONIBLE INSUFICIENTE'; break;
            case '118': $detail = 'TARJETA NO REGISTRADA'; break;
            case '119': $detail = 'DENEGACION SIN ESPECIFICAR EL MOTIVO'; break;
            case '125': $detail = 'TARJETA NO EFECTIVA'; break;
            case '129': $detail = 'ERROR CVV2/CVC2'; break;
            case '167': $detail = 'CONTACTAR CON EL EMISOR: SOSPECHA DE FRAUDE'; break;
            case '180': $detail = 'TARJETA AJENA AL SERVICIO'; break;
            case '181': 
            case '182': $detail = 'TARJETA CON RESTRICCIONES DE DEBITO O CREDITO'; break;
            case '184': $detail = 'ERROR EN AUTENTICACION'; break;
            case '190': $detail = 'DENEGACION SIN ESPECIFICAR EL MOTIVO'; break;
            case '191': $detail = 'FECHA DE CADUCIDAD ERRONEA'; break;
        }
    }
?>
<div id="main" style="padding:200px;">    
    <!-- /section -->            
    <p align="center"><i class="fa fa-remove fa-5x" style="color:red"></i></p>
    <p align="center">Ha ocurrido un error al procesar su pago </p>
    <p align="center"><b>RESULTADO: </b><?= $detail ?></p>
</div>


<?php $this->load->view('includes/template/footer') ?>
<!-- /footer -->