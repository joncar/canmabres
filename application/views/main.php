<?php $this->load->view('includes/template/header') ?>
<div id="main">
    <?php $this->load->view('includes/template/banner'); ?>
    <!-- /section -->
            <section class="border-bottom">
                <div class="container">
                    <div class="policy-wrapper">

                        <div class="row">

                            <div class="col-md-4 col-sm-4 col-xs-8">
                                <div class="policy">
                                    <div class="policy-icon">
                                        <i class="fa fa-street-view"></i>
                                    </div>

                                    <div class="policy-text">
                                        <h4>TOTS EL NOSTRES PRODUCTES</h4>
                                        <p>SON de COMERÇ DE PROXIMITAT</p>
                                    </div>
                                </div>
                                <!-- /.policy -->
                            </div>

                            <div class="col-md-4 col-sm-4 col-xs-8">
                                <div class="policy">
                                    <div class="policy-icon">
                                        <i class="fa fa-truck"></i>
                                    </div>

                                    <div class="policy-text">
                                        <h4>ET PORTEM LA TEVA COMANDA</h4>
                                        <p>comanda minima de 45€</p>
                                    </div>
                                </div>
                                <!-- /.policy -->
                            </div>

                            <div class="col-md-4 col-sm-4 col-xs-8">
                                <div class="policy">
                                    <div class="policy-icon">
                                        <i class="fa fa-leaf"></i>
                                    </div>

                                    <div class="policy-text">
                                        <h4>100% CARN ECOLÒGICA</h4>
                                        <p>ESTIMEM MOLT ELS NOSTRES ANIMALS</p>
                                    </div>
                                </div>
                                <!-- /.policy -->
                                
                            </div>
                        </div>
                        <!-- /.row -->

                    </div>
                    <!-- /.policy-wrapper -->
                </div>
                <!-- /.container -->
            </section>
            <!-- /section -->
            <div class="row" style="margin-top: 32px;">
              
            <?php $this->load->view('includes/template/home_products'); ?>
            <!-- /section -->                        

        </div>


        <?php $this->load->view('includes/template/footer') ?>
        <!-- /footer -->

    </div>
    <!-- /#wrapper -->