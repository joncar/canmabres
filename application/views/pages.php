<div class="animated-all  sidebar-menu-wrap  ">
<?php $this->load->view('includes/template/nav') ?>
<!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="home-page">
                <div id="ip-container" class="ip-container">
                    <header class="ip-header">
                      <div class="ip-loader">
                        <div class="ip-logo "> 
                            <a title="pixtheme" href="http://pix-theme.com/wordpress/xsport">
                                  <?= img('img/logo.png') ?>
                          <div class="logo-desc"> </div>
                                  </a> </div>
                        <svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
                        <path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,39.3,10z"/>
                        <path id="ip-loader-circle" class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                        </svg> </div>
                    </header>
                </div>
                <!-- Loader -->
                <!-- Loader end -->
                <div class="header shop-header">
                    <div class="container">
                        <div class="row">
                            <div class="top-header">
                                <div class="info-top col-md-6 text-left"> <i class="fa fa-phone"></i> 24/7 SUPPORT &nbsp; &nbsp; <a href="">0800 123 4567</a> </div>
                                <div class="info-top col-md-6 text-right">
                                    <ul id="menu-top" class="">
                                        <li id="menu-item-2198" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2198 dropdown">
                                            <a href="http://pix-theme.com/wordpress/xsport/my-account/" class="external">My Account</a>
                                        </li>
                                        <li id="menu-item-2199" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2199 dropdown">
                                            <a href="http://pix-theme.com/wordpress/xsport/checkout/" class="external">Checkout</a>
                                        </li>
                                        <li id="menu-item-2367" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2367 dropdown">
                                            <a href="http://pix-theme.com/wordpress/xsport/cart/" class="external">Cart</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section class="shop-section ">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3 col-md-12 col-xs-12 mobile-center">
                                    <div class="blog-logo text-left"> 
                                        <a title="pixtheme" href="http://pix-theme.com/wordpress/xsport" id="logo" class="logo ">
                                            <?= img('img/logo.png') ?>
                                            <div class="logo-desc"> </div>
                                        </a> 
                                    </div>
                                </div>
                                <div class="col-md-6  col-xs-12 mobile-center">
                                    <form action="http://pix-theme.com/wordpress/xsport" method="get" id="search-global-form">
                                        <div class="input-group top-search">
                                            <div class="input-group-btn search-panel">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span id="search_concept">Filter by</span> <span class="caret"></span> 
                                                </button>
                                                <ul class="dropdown-menu" id="search_filter_header" role="menu">
                                                    <li><a href="#blog">BLOG</a></li>
                                                    <li><a href="#basketball">Basketball</a></li>
                                                    <li><a href="#bikes">Bikes</a></li><li><a href="#fitness">Fitness</a></li>
                                                    <li><a href="#football">Football</a></li><li><a href="#hockey">Hockey</a></li>
                                                    <li><a href="#rugby">Rugby</a></li><li><a href="#tennis">Tennis</a></li>
                                                </ul>
                                                <input type="hidden" id="search_filter_header_input" name="product_cat" value="">
                                                <input type="hidden" id="search_filter_header_type" name="post_type" value="post"> 
                                            </div>
                                            <input type="text" class="form-control" value="" name="s" placeholder="Press Enter to search">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                                            </span> 
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-3 col-xs-12 text-right mobile-center">
                                    <div class="popover-shorty"> 
                                        <a  href="http://pix-theme.com/wordpress/xsport/cart/"> 
                                            <span class="qty-top-cart-active">0</span> <i class="fa fa-shopping-cart"></i> 
                                        </a>  
                                        <div class="popover popover-cart bottom">
                                            <div class="empty-cart">No products in the cart.</div>                                               
                                        </div>
                                    </div>
                                    <div class="popover-shorty"> <a  href="http://facebook.com/"><i class="fa fa fa-truck"></i> </a>
                                        <div class="popover bottom">
                                            <div class="arrow"></div>
                                            <h3 class="popover-title">Facebook follow </h3>
                                            <div class="popover-content"> Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. </div>
                                        </div>
                                    </div>
                                    <div class="popover-shorty"> <a  href="http://thumbs.com/"><i class="fa fa-thumbs-o-up"></i> </a>
                                        <div class="popover bottom">
                                            <div class="arrow"></div>
                                            <div class="popover-content"> Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. </div>
                                        </div>
                                    </div>
                                    <div class="popover-shorty"> <a  href="http://truck.com/"><i class="fa fa-truck"></i> </a>
                                        <div class="popover bottom">
                                            <div class="arrow"></div>
                                            <h3 class="popover-title">Reliable delivery</h3>
                                            <div class="popover-content"> Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. </div>
                                        </div>
                                    </div>
                                </div>      
                            </div>
                        </div>
                    </section>
                        <?php $this->load->view('includes/template/menu'); ?>
                </div>

<!--- END TOP NAV --> 

<!--#content-->
<div class="content" id="content">
<!-- HEADER -->
<?= $texto ?>
<?= $this->load->view('includes/template/footer') ?>
</div>
<!--#page-content-wrapper-->
</div></div> 
<!--#content-->
<!-- end layout-theme -->
</div> 


<div class="la-anim-1"></div>