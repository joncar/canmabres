<?php ini_set('display_errors',true); ?>
<?php $total = $venta->total ?>
<?php 
    // Se crea Objeto
    $miObj = new RedsysAPI;

    // Valores de entrada
    //$merchantCode 	="327977559";
    $merchantCode 	="223174525";
    $key                ='uhsuJ4a3ZGU5aP3njsIYOoob/IL3BwJb';//Clave secreta del terminal
    $terminal 		="1";
    $amount 		=$total*100;    
    $currency 		="978";
    $transactionType    ="0";
    $merchantURL 	=base_url('main/procesarpago');
    $urlOK 		=base_url('main/pagoOk');
    $urlKO 		=base_url('main/pagoKo');
    $order 		=$venta->transactionid;

    //Entorno
    //$urlPago = "https://sis.redsys.es/sis/realizarPago"; //ENTORNO REAL    

    // Se Rellenan los campos
    $miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
    $miObj->setParameter("DS_MERCHANT_ORDER",strval($order));
    $miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$merchantCode);
    $miObj->setParameter("DS_MERCHANT_CURRENCY",$currency);
    $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$transactionType);
    $miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
    $miObj->setParameter("DS_MERCHANT_MERCHANTURL",$merchantURL);
    $miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);		
    $miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);
        
    $version="HMAC_SHA256_V1";
    //$key = 'xqSeBn4Qp+EsCTtko9MwNEGqFnMoDj2d';//Clave secreta del terminal    
    $request = "";
    $params = $miObj->createMerchantParameters();
    $signature = $miObj->createMerchantSignature($key);
?>

<div class="page-header">
        <h1>
                Finalitzar compra
                <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        
                </small>
        </h1>
</div><!-- /.page-header -->

<div class="row">
        <div class="col-xs-12">
               <?php $this->db->select('ventas_detalles.*, productos.*, ventas_detalles.gramaje as gramajeselected'); ?>
               <?php $this->db->join('productos','productos.id = ventas_detalles.productos_id'); ?>
            <div class="row">
               <?php foreach($this->db->get_where('ventas_detalles',array('ventas_id'=>$venta->id))->result() as $c): ?>                            
                        <div class="col-xs-12 col-sm-4" style="margin-bottom: 20px">
                                <div class="col-xs-6 col-sm-3">
                                    <a href="<?= site_url('productos/'. toURL($c->producto_nombre).'-'.$c->id) ?>" title="">
                                        <?= img('images/productos/'.$c->foto,'width:100%') ?>
                                    </a>
                                </div>
                                <div class="col-xs-6 col-sm-8">
                                    <div class="whishlist-name">
                                        <h3><a href="<?= site_url('productos/'. toURL($c->producto_nombre).'-'.$c->id) ?>" title=""><?= $c->producto_nombre ?></a></h3>
                                    </div>
                                    <div class="whishlist-price">
                                        <span>Preu:</span>
                                        <span style="font-size:20px"><strong><?= moneda($c->precio) ?></strong></span>
                                    </div>
                                    <?php if(!empty($c->gramajeselected)): ?>
                                        <div class="whishlist-price">
                                            <span>Gramaje:</span>
                                            <span style="font-size:20px"><strong><?= $c->gramajeselected ?></strong></span>
                                        </div>
                                    <?php endif ?>
                                    <div class="whishlist-quantity">
                                        <span>Quantitat:</span>
                                        <span><?= $c->cantidad ?></span>
                                    </div>
                                </div>                                
                            </div>                            
                    <?php endforeach ?>                             
            </div>
                    <div class="menu-cart-total" style="text-align:right">
                        <div>
                            <span style="font-family: roboto;">Lot</span>
                            <span style="font-family: roboto;"><?= moneda($venta->total-$venta->costo_envio) ?></span>
                        </div>
                        <div>
                            <span style="font-family: roboto;">Cost enviament</span>
                            <span style="font-family: roboto;" id="costo_envio"><?= moneda($venta->costo_envio) ?></span>
                        </div>
                        <div>
                            <span style="font-family: roboto;">Total</span>
                            <span style="font-size:40px; font-family: montserratBold" id="total"><?= moneda($venta->total) ?></span>
                        </div>
                    </div>
                    <div>                                                
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" <?= ($venta->forma_pago=='Transferencia')?'class="active"':'' ?>><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Transferència</a></li>
                          <li role="presentation" <?= ($venta->forma_pago=='Contrareembols')?'class="active"':'' ?>><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Contrareembols</a></li>
                          <li role="presentation" <?= ($venta->forma_pago=='Visa')?'class="active"':'' ?>><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Targeta de crédit</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane <?= ($venta->forma_pago=='Transferencia')?'active':'' ?>" id="home">
                                <?= str_replace('{id}',$venta->id,$this->db->get_where('notificaciones',array('id'=>6))->row()->texto) ?>
                            </div>
                            <div role="tabpanel" class="tab-pane <?= ($venta->forma_pago=='Contrareembols')?'active':'' ?>" id="profile">
                                <?= str_replace('{id}',$venta->id,$this->db->get_where('notificaciones',array('id'=>7))->row()->texto) ?>
                            </div>
                            <div role="tabpanel" class="tab-pane <?= ($venta->forma_pago=='Visa')?'active':'' ?>" id="messages">
                                <form id="formPay" action="https://sis.redsys.es/sis/realizarPago" method="post">
                                  <?php $total = $venta->total*100 ?>
                                  <input type="hidden" name="Ds_SignatureVersion" value="<?php echo $version; ?>"/>
                                  <input type="hidden" name="Ds_MerchantParameters" value="<?php echo $params; ?>"/>
                                  <input type="hidden" name="Ds_Signature" value="<?php echo $signature; ?>" />
                                  <div class="form-row place-order" align="center">
                                      <?= img('img/pay.jpg','max-width:100%') ?><br/>
                                      <button type="submit" title="" class="btn btn-lg btn-primary">Finalitzar compra</button>
                                  </div>
                                  <?php 
                                      $_SESSION['productocompra'] = $venta->id;
                                  ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                
        </div><!-- /.col -->
</div><!-- /.row -->
