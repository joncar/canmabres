<?php $this->load->view('includes/template/header') ?>
<div id="main">    
    <div class="main-header background background-image-heading-product">
        <div class="container">
            <h1>CISTELL DE COMPRA</h1>
        </div>
    </div>        
    <div class="submenu megamenu">
        <div>
            <div class="container" style="padding:40px;">
                <form action="" id="form" class="form-horizontal" onsubmit="return refresh()">
                    <?php $this->load->view('includes/fragmentos/carritoForm'); ?>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('includes/template/footer') ?>
<script>
    function refresh(){
        form = new FormData(document.getElementById('form'));
        $.ajax({
            url:'<?= base_url('main/addToCartArray') ?>',
            data:form,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST'            
        }).always(function(data){
            if(data==='success'){
                document.location.href="<?= empty($_SESSION['user'])?site_url('registro/index/add?redirect=usuario/comprar'):base_url('usuario/comprar') ?>";
            }else{
                emergente(data);
            }
        });
        return false;
    }
    
    function delToCart(producto_id){
        $.post('<?= base_url() ?>main/delToCart/'+producto_id,{},function(data){
            $(".menubar-cart").html(data);
             $.post('<?= base_url() ?>main/refreshCartForm',{},function(data){
                 $("#form").html(data);
            });
        });
    }
    
    function politicas(){
        $.post('<?= base_url() ?>main/mostrarlic',{},function(data){
                 emergente(data);
        });
    }
</script>