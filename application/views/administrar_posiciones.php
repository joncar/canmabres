<html lang="es">
    <head>
        <title>Certificado</title>
        <meta charset="utf-8">
        <script src="http://code.jquery.com/jquery-1.10.0.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>                
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="http://telefarma.com.py/sysfarma/js/frame.js"></script>    
        <link href="http://telefarma.com.py/sysfarma/css/impresiones/imprimir_factura.css" rel="stylesheet" type="text/css">
        <script>
            var script = {};
            var len = $("div").length;
            var i = 0;
            $(document).ready(function(){
                $('div').draggable({
                        stop:function(){
                            i = 0;
                            $("div").each(function(){                                
                                if(i==len){
                                    console.log(script);
                                }
                                script[$(this).data('val')] = {top:$(this).css('top'),left:$(this).css('left')}                                
                                i++;
                            });
                        }
                        }).appendTo('body').css('position','absolute');
                
                        $(document).on('keyup','body',function(event){        
                            if(event.which=='113'){
                                element = $("body").html();
                                emergente(element);
                            }
                        });
                    });
                    
                    function guardar(){
                        $.post('<?= base_url('admin/ajustes_sistema/eventos/update_validation/'.$evento->id) ?>/posiciones',{posiciones:JSON.stringify(script)},function(data){
                            data = data.replace('<textarea>','');
                            data = data.replace('</textarea>','');
                            data = JSON.parse(data);
                            if(data.success){
                                $.post('<?= base_url('admin/ajustes_sistema/eventos/update/'.$evento->id) ?>/posiciones',{posiciones:JSON.stringify(script)},function(data){
                                    data = data.replace('<textarea>','');
                                    data = data.replace('</textarea>','');
                                    data = JSON.parse(data);
                                    if(data.success){
                                        alert('Datos guardados');
                                    }
                                });
                            }
                        });
                    }
        </script>
</head>
    <?php 
        $posiciones = json_decode($evento->posiciones);         
    ?>
        <body style="background:url(<?= base_url('images/certificados/'.$evento->certificado) ?>) no-repeat">
            <div style="top:<?= $posiciones->nombre->top ?>; left:<?= $posiciones->nombre->left ?>;" data-val="nombre">{nombre}</div>
                <div style="top:<?= $posiciones->apellido_paterno->top ?>; left:<?= $posiciones->apellido_paterno->left ?>;" data-val="apellido_paterno">{apellido_paterno}</div>
                <div style="top:<?= $posiciones->titulo->top ?>; left:<?= $posiciones->titulo->left ?>;" data-val='titulo'>{titulo}</div>
                <div style="top:<?= $posiciones->fecha->top ?>; left:<?= $posiciones->fecha->left ?>;" data-val='fecha'>{fecha}</div>
                <div style="top:<?= $posiciones->chip->top ?>; left:<?= $posiciones->chip->left ?>;" data-val='chip'>{chip}</div>
                <div style="top:<?= $posiciones->grupo->top ?>; left:<?= $posiciones->grupo->left ?>;" data-val='grupo'>{grupo}</div>
                <div style="top:<?= $posiciones->categoria->top ?>; left:<?= $posiciones->categoria->left ?>;" data-val='categoria'>{categoria}</div>
                <div style="top:<?= $posiciones->tiempo_oficial->top ?>; left:<?= $posiciones->tiempo_oficial->left ?>;" data-val='tiempo_oficial'>{tiempo_oficial}</div>
                <div style="top:<?= $posiciones->tiempo_chip->top ?>; left:<?= $posiciones->tiempo_chip->left ?>;" data-val='tiempo_chip'>{tiempo_chip}</div>
                <div style="top:<?= $posiciones->lugar_obtenido->top ?>; left:<?= $posiciones->lugar_obtenido->left ?>;" data-val='lugar_obtenido'>{lugar_obtenido}</div>
                <aside style="position:absolute; bottom:0px; left:0px;"><a href="javascript:guardar()" class="btn btn-success">Guardar</a></aside>
        </body>
</html>
