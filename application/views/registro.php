<?php $this->load->view('includes/template/header') ?>
<?php $this->load->view('includes/template/banner'); ?>
		<div class="container-fluid" style="margin-top:100px">
		    <div class="row">
		        <div class="col-xs-12 col-sm-4 col-sm-offset-1">
		            <?php $this->load->view('predesign/login') ?>
		        </div>
		        <div class="col-xs-12 col-sm-6">
		            <?= $output ?>            
		        </div>
		    </div>
		</div>
    <?php $this->load->view('includes/template/footer') ?>
    <!-- /footer -->
    <?php 
    if(!empty($js_files)):
    foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
    <?php endif; ?>