<nav class="menu-main-menu-container">
    <ul id="menu-main" class="cd-primary-nav">
        <li class="menu-item"><a href="#">Home</a>
            <ul class="sub-menu">
                    <li class="menu-item  current-menu-item"><a href="wordpress/">Home version 1</a></li>
                    <li class="menu-item"><a href="wordpress/home-2/">Home version 2</a></li>
                    <li class="menu-item"><a href="wordpress/home-3/">Home version 3</a></li>
            </ul>
        </li>
        <li id="menu-item-20" class="menu-item  menu-item-20"><a href="wordpress/blog/">Blog</a></li>
        <li id="menu-item-19" class="menu-item  menu-item-19"><a href="wordpress/faqs/">FAQs</a></li>
        <li id="9" class="menu-item  9"><a href="wordpress/about-2/">About</a></li>
        <li id="menu-item-230" class="menu-item  menu-item-230"><a href="wordpress/contact-2/">Contact</a></li>
    </ul>
 </nav>