<?php $carrito = $this->querys->getCarrito(); $total = 0;?>
<div class="row">
<?php foreach($carrito as $c): ?>
<div class="col-xs-12 col-sm-6" style="margin-bottom: 20px">
    <div class="col-xs-1">
        <a href="javascript:delToCart('<?= $c->id ?>')" title="" class="remove">
            <i class="icon icon-remove"></i>
        </a>
    </div>
    <div class="col-xs-12 col-sm-3">
        <a href="<?= site_url('productos/'. toURL($c->producto_nombre).'-'.$c->id) ?>" title="">
            <?= img('images/productos/'.$c->foto,'width:100%;') ?>
        </a>
    </div>
    <div class="col-xs-12 col-sm-8">
        <div class="whishlist-name" style="margin-top:20px">
            <h3 style="margin:0px;"><a href="<?= site_url('productos/'. toURL($c->producto_nombre).'-'.$c->id) ?>" title=""><?= $c->producto_nombre ?></a></h3>
        </div>
        <div class="form-group">
            <label for="preu" class="col-sm-4 control-label">Preu:</label>
            <div class="col-sm-5">
                <span style="font-size:20px"><strong><?= moneda($c->precio) ?></strong></span>
            </div>
        </div> 
        <div class="form-group">
            <label for="cantidad" class="col-sm-4 control-label">Quantitat:</label>
            <div class="col-sm-5">
                <input type="number" name="cantidad[]" class="form-control" value="<?= $c->cantidad ?>">
                <input type="hidden" name="id[]" class="cantidad" value="<?= $c->id ?>">
            </div>
        </div> 
        <?php if(!empty($c->gramaje)): ?>
            <!--<div class="form-group">
                <label for="gramaje" class="col-sm-4 control-label">Gramatge*:</label>
                <div class="col-sm-5">
                    <?php 
                        $items = explode(',',$c->gramaje); 
                        echo '<select name="gramaje[]" id="gramaje" class="form-control">';
                        echo '<option value="">Tria un gramatge</option>';
                        foreach($items as $i){
                            if(!empty($c->gramajeselected) && $c->gramajeselected==$i){
                                echo '<option selected>'.$i.'</option>';
                            }else{
                                echo '<option>'.$i.'</option>';
                            }
                        }
                        echo '</select>';
                    ?>
                </div>
            </div>-->
        <?php else: ?>
        <!--<input type="hidden" name="gramaje[]" id="gramaje" value=" ">-->
        <?php endif ?>
        <input type="hidden" name="gramaje[]" id="gramaje" value=" ">
    </div>                                
</div>
<?php $total+= ($c->cantidad*$c->precio); ?>
<?php endforeach ?>
</div>
<?php if($total>0): ?>
<div class="row menu-cart-total" style="text-align:right">
    <div class="col-xs-12 col-sm-9">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="provincia" class="col-xs-12 col-sm-4 control-label" style="text-align:left">Provincia entrega*</label>
                <div class="col-xs-12 col-sm-8">
                    <select id="provincia" name="provincias_id" class='form-control'>
                        <option selected="selected" value="">Tria la teva provincia </option>
                        <?php foreach($this->db->get('provincias')->result() as $p): ?>
                        <option data-val="<?= $p->costo_envio ?>" value="<?= $p->id ?>"><?= $p->nombre_provincia ?> <?= moneda($p->costo_envio) ?></option>
                        <?php endforeach ?>
                   </select>
                </div>
            </div>
            <div class="form-group">
                <label for="dia_entrega" class="col-xs-12 col-sm-4 control-label" style="text-align:left">Dia d'entrega*</label>
                <div class="col-xs-12 col-sm-8">
                    <select id='dia_entrega' name="dia_entrega" class='form-control'>
                        <option selected="selected" value="">Tria un dia d'entrega</option>
                        <option>Dimecres</option>
                        <option>Dijous</option>
                        <option>Divendres</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="hora_entrega" class="col-xs-12 col-sm-4 control-label" style="text-align:left">Horari d'entrega*</label>
                <div class="col-xs-12 col-sm-8">
                    <select id="hora_entrega" name="hora_entrega" class='form-control'>
                        <option selected="selected" value="">Tria un horari per fer l'entrega</option>
                        <option>Tot el dia de 9h a 23h</option>
                        <option>Matins de 9h a 14h</option>
                        <option>Migdia de 14h a 16h</option>
                        <option>Tarda de 16h a 18h</option>
                        <option>Tarda/vespre de 18 a 23h</option>
                   </select>
                </div>
            </div>
            <div class="form-group">
                <label for="forma_pago" class="col-xs-12 col-sm-4 control-label" style="text-align:left">Forma pagament*</label>
                <div class="col-xs-12 col-sm-8">
                    <select id="forma_pago" name="forma_pago" class='form-control'>
                        <option selected="selected" value="">Tria una forma de pagament</option>
                        <option>Transferencia</option>
                        <option>Visa</option>
                        <option> Contrareembols</option>
                    </select>
                </div>
            </div>
            <div class="field field_aceptat">
                <input type="checkbox" id="politicas" name="politicas">
                <label for="politicas">Acepto les</label>&nbsp;<a href="javascript:politicas()">Condicions de venda *</a><span class="error"></span>                
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <textarea name='observaciones' class="form-control" placeholder="Observacions" style="height:181px;"></textarea>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3" align="right" style="margin-top:30px">
        <div>
            <span style="font-family: roboto;">Lot</span>
            <span style="font-family: roboto;"><?= moneda($total) ?></span>
        </div>
        <div>
            <span style="font-family: roboto;">Cost enviament</span>
            <span style="font-family: roboto;" id="costo_envio"><?= moneda(0) ?></span>
        </div>
        <div>
            <span style="font-family: roboto;">Total</span>
            <span style="font-size:40px; font-family: montserratBold" id="total"><?= moneda($total) ?></span>
        </div>
    </div>
</div>
<div align="right">
<!--<a href="<?= empty($_SESSION['user'])?site_url('registro/index/add?redirect=usuario/comprar'):base_url('usuario/comprar') ?>" title="" class="btn btn-lg btn-primary">Proceder a pagar</a>-->    
    <input type="hidden" name="costo_envio" id="costo_envio_form" value="0">
<button type="submit" class="btn btn-lg btn-primary">FINALITZAR COMPRA</button>
</div>
<?php else: ?>
<div class="menu-cart-total">
<span>Carret buit</span>                    
</div>
<?php endif ?>
<script>
    var total = <?= $total ?>;
    $(document).on('ready',function(){
       $("#provincia").on('change',function(){
           //alert(total+$("#provincia :selected").data('val'));
           var costo_envio = parseFloat($("#provincia :selected").data('val'));
           var t = total+parseFloat($("#provincia :selected").data('val'));
           $("#costo_envio").html(costo_envio.formatMoney(2,',','.')+' €');
           $("#total").html(t.formatMoney(2,',','.')+' €');
           $("#costo_envio_form").val(costo_envio);
       }) 
    });
</script>