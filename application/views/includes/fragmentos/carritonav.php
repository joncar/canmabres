<?php $carrito = $this->querys->getCarrito(); $total = 0;?>
<a href="#" title="" class="awemenu-icon menu-shopping-cart">
    <i class="icon icon-shopping-bag"></i>
    <span class="awe-hidden-text">Carrito</span>

    <?php if(count($carrito)>0): ?>
        <span class="cart-number"><?= count($carrito) ?></span>
    <?php endif ?>
</a>

<ul class="submenu megamenu">
    <li>
        <div class="container-fluid">
            <ul class="whishlist">
                <?php foreach($carrito as $c): ?>
                    <li>
                        <div class="whishlist-item">
                            <div class="product-image">
                                <a href="<?= site_url('productos/'. toURL($c->producto_nombre).'-'.$c->id) ?>" title="">
                                    <?= img('images/productos/'.$c->foto) ?>
                                </a>
                            </div>
                            <div class="product-body">
                                <div class="whishlist-name">
                                    <h3><a href="<?= site_url('productos/'. toURL($c->producto_nombre).'-'.$c->id) ?>" title=""><?= $c->producto_nombre ?></a></h3>
                                </div>
                                <div class="whishlist-price">
                                    <span>Preu:</span>
                                    <strong><?= moneda($c->precio) ?></strong>                                    
                                </div>

                                <div class="whishlist-quantity">
                                    <span>Quantitat:</span>
                                    <span><?= $c->cantidad ?></span>
                                </div>
                            </div>
                        </div>
                        <a href="javascript:remToCart('<?= $c->id ?>')" title="" class="remove">
                            <i class="icon icon-remove"></i>
                        </a>                        
                    </li>
                    <?php $total+= ($c->cantidad*$c->precio); ?>
                <?php endforeach ?>
            </ul>
            <?php if($total>0): ?>
                <div class="menu-cart-total">
                    <span>Total</span>
                    <span class="price"><?= moneda($total) ?></span>
                </div>
                <div class="cart-action">
                    <a href="<?= base_url('main/carrito') ?>" title="" class="btn btn-lg btn-dark btn-outline btn-block">VEURE CISTELL</a>
                    <a href="<?= base_url('main/carrito') ?>" title="" class="btn btn-lg btn-primary btn-block">FINALITZAR COMPRA</a>
                </div>
            <?php else: ?>
                <div class="menu-cart-total">
                    <span>Carret buit</span>                    
                </div>
            <?php endif ?>
        </div>
    </li>
</ul>