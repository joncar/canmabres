<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">El teu tauler</span>
                </a>
                <b class="arrow"></b>
            </li>
            <li class="highlight">
                <a href="<?= site_url('usuario/ventas') ?>">
                        <i class="menu-icon fa fa-user"></i>
                        <span class="menu-text">Compres</span>
                </a>
                <b class="arrow"></b>
            </li>   
            <!--- Admin --->
            <?php if($this->user->admin==1): ?>
            <li>
                <a class="dropdown-toggle" href="#">
                        <i class="menu-icon fa fa-lock"></i>
                        <span class="menu-text">Admin</span>
                        <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="highlight">
                        <a href="<?= site_url('admin/ajustes_sistema/ajustes') ?>">
                                <i class="menu-icon fa fa-wrench"></i>
                                <span class="menu-text">Ajustes</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="highlight">
                        <a href="<?= site_url('admin/ajustes_sistema/provincias') ?>">
                                <i class="menu-icon fa fa-wrench"></i>
                                <span class="menu-text">Provincias</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="highlight">
                        <a href="<?= site_url('admin/ajustes_sistema/notificaciones') ?>">
                                <i class="menu-icon fa fa-user"></i>
                                <span class="menu-text">Notificaciones</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="highlight">
                        <a href="<?= site_url('admin/ajustes_sistema/banner') ?>">
                                <i class="menu-icon fa fa-wrench"></i>
                                <span class="menu-text">Banner principal</span>
                        </a>
                        <b class="arrow"></b>
                    </li>  
                    <li class="highlight">
                        <a href="<?= site_url('admin/pages/paginas') ?>">
                                <i class="menu-icon fa fa-file-o"></i>
                                <span class="menu-text">Paginas</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="highlight">
                        <a href="<?= site_url('admin/productosctrl/categorias') ?>">
                                <i class="menu-icon fa fa-file-o"></i>
                                <span class="menu-text">Categorias</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="highlight">
                        <a href="<?= site_url('admin/ajustes_sistema/ventas') ?>">
                                <i class="menu-icon fa fa-user"></i>
                                <span class="menu-text">Ventas</span>
                        </a>
                        <b class="arrow"></b>
                    </li>                    
                    <!--- Seguridad --->
                    <li>
                        <a class="dropdown-toggle" href="#">
                            <i class="menu-icon fa fa-lock"></i>
                            <span class="menu-text">Seguridad</span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class='submenu'>
                            <li>
                                <a href="<?= base_url('seguridad/grupos') ?>">
                                    <i class="menu-icon fa fa-users"></i>
                                    <span class="menu-text">Grupos</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/funciones') ?>">
                                    <i class="menu-icon fa fa-arrows"></i>
                                    <span class="menu-text">Funciones</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/user') ?>">
                                    <i class="menu-icon fa fa-user"></i>
                                    <span class="menu-text">Usuarios</span>                                    
                                </a>                                
                            </li>
                        </ul>
                    </li>
                    <!--- Fin seguridad ---->
                </ul>
            </li>
            <!--- Fin Admin ---->
            <?php endif ?>
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
