<?php if(!empty($banner) && $banner->num_rows()>0): ?>
<section>
<div class="main-slider-wrapper">
    <div class="main-slider owl-carousel owl-carousel-inset">        
        <?php foreach($banner->result() as $b): ?>
        <div class="main-slider-item">
            <div class="main-slider-image">
                <?= img('images/banner/'.$b->foto) ?>
            </div>

            <div class="main-slider-text">
                <div class="fp-table">
                    <div class="fp-table-cell center">
                        <div class="container">
                            <h3><?= $b->subtitulo ?></h3>
                            <h2><?= $b->titulo ?></h2>
                            <?php if(!empty($b->enlace)): ?>
                                <div class="button">
                                    <a href="<?= $b->enlace ?>" class="btn btn-lg btn-primary margin-right-15">Comprar ahora</a>                                    
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach ?>
    </div>
</div>

<script>
    $(function() {  aweMainSlider(); });
</script>

</section>
<?php endif ?>