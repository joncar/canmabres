<section>
    <div class="container">

        <div class="home-products">
            <?php if(!empty($this->ajustes->banner_main)): ?>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="<?= $this->ajustes->banner_link_main ?>">
                            <img class='hidden-xs hidden-sm' src="<?= base_url('img/'.$this->ajustes->banner_main) ?>" alt="" style="width:100%;">
                            <img class='visible-xs visible-sm' src="<?= base_url('img/'.$this->ajustes->banner_main_movil) ?>" alt="" style="width:100%;">
                        </a>
                      </div>
                    </div>
                </div>
            <?php endif ?>
            <?php 
                foreach($this->querys->getCategorias()->result() as $c): 
            ?>
                
                <?php 
                    $this->db->where('categorias_id',$c->id);
                    if(empty($_GET)){
                        $this->db->limit('9');
                    }
                    $producto = $this->querys->getProductos();                     
                    if($producto->num_rows()>0):                     
                ?>
            <div class="row" style="border-bottom:1px solid #c9c9c9; margin-top:70px; margin-left: 0; margin-right: 0">
                
                <div class="col-md-3 col-sm-4">
                    <div class="awe-media home-cate-media">
                        <div class="awe-media-header">
                            <div class="awe-media-image">
                                <img src='<?= base_url('images/categorias/'.$c->foto) ?>' class="current">                                
                            </div>
                            <!-- /.awe-media-image -->

                            <div class="awe-media-overlay overlay-dark-50 fullpage">
                                <div class="content">
                                    <div class="fp-table text-left">
                                        <div class="fp-table-cell">

                                            <h2 class="upper"><?= $c->categoria_nombre ?></h2>                                            
                                            <a href="<?= base_url('categorias/'.toURL($c->categoria_nombre).'-'.$c->id) ?>" class="btn btn-sm btn-outline btn-white">Veure Tots</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.awe-media-overlay -->
                        </div>
                        <!-- /.awe-media-header -->
                    </div>
                    <!-- /.awe-media -->
                </div>                
                <div class="col-md-9 col-sm-8">
                    <div class="products">
                        <?php foreach($producto->result() as $p):  ?>
                            <?php $this->load->view('includes/fragmentos/producto',array('p'=>$p)); ?>
                        <?php endforeach ?>
                        <div class="col-xs-12" style="text-align: center;margin: -70px 0 21px;">
                            <a href="<?= base_url('categorias/'.toURL($c->categoria_nombre).'-'.$c->id) ?>" class="btn btn-lg btn-primary">Veure tots</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif ?>
            <!-- /.row -->
            <?php endforeach ?>
        </div>
        <!-- /.home-products -->

    </div>
    <!-- /.container -->
</section>